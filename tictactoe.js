"use strict";
document.addEventListener('DOMContentLoaded', () => {
    let curPlayer = "X";
    const endMessage = document.querySelector('aside');
    const table = document.querySelector('#gameboard');
    const playerMsg = document.querySelector('#curplayer');
    let winStatus = false;
    const positions = [];
    let tableCells = document.getElementsByTagName('td');

    const playerSpace = document.querySelector('#winningplayer');
    function showWinningMessage(winner){
        if(winner === false){
            return;
        }
        else {
            playerSpace.innerText = winner;
            endMessage.style.visibility = 'visible';
        }
    }

    table.addEventListener('click', (e) => {
        if(e.target.tagName === 'TD'){
            if(e.target.innerText === 'X' || e.target.innerText === 'O'){
                return;
            }
            else if (winStatus !== false){
                return;
            }
            e.target.textContent = curPlayer;
            if (curPlayer === "X"){
                e.target.classList.add("xsquare");
                curPlayer = "O";
                playerMsg.innerText = "O";
            }
            else if (curPlayer === "O"){
                e.target.classList.add("osquare");
                curPlayer = "X";
                playerMsg.innerText = "X";
            }
            for (let i = 0; i<9; i++){
                let currText = tableCells[i].textContent;
                if(currText === ''){
                    positions[i] = false;
                }
                else{
                    positions[i] = currText;
                }
            }
            winStatus = checkBoard(...positions);
            console.log(winStatus);
        }
        showWinningMessage(winStatus);
    });
    const newGame = document.querySelector('#reset');
    newGame.addEventListener('click', () => {
        for (let i =0; i<9; i++){
            tableCells[i].classList.remove("osquare");
            tableCells[i].classList.remove("xsquare");
            tableCells[i].innerText = "";
        }
        playerSpace.innerText = "";
        endMessage.style.visibility = 'hidden';
        curPlayer = "X";
        playerMsg.innerText = "X";
        console.log(curPlayer);
        winStatus = false;
    });














})
// Put your DOMContentLoaded event listener here first.